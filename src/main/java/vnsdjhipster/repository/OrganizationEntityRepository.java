package vnsdjhipster.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vnsdjhipster.domain.OrganizationEntity;


/**
 * Spring Data  repository for the OrganizationEntity entity.
 */
@Repository
public interface OrganizationEntityRepository extends JpaRepository<OrganizationEntity, Long> {

}
