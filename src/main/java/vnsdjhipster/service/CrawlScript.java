package vnsdjhipster.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vnsdjhipster.domain.OrganizationEntity;
import vnsdjhipster.repository.OrganizationEntityRepository;

import javax.transaction.Transactional;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

@Service
@Transactional
public class CrawlScript {

    @Autowired
    private OrganizationEntityRepository organizationEntityRepository;


    public  ArrayList readCSV() throws IOException {
        BufferedReader csvReader = new BufferedReader(new FileReader("C:\\Users\\phipnguyen2\\Desktop\\Workspace\\vnsdjhipster\\src\\main\\resources\\csv\\odm_org_(first_2018_vietnoys).csv"));
        ArrayList allUUID = new ArrayList();

        String row;
        int i = 0;
        while ((row = csvReader.readLine() )!= null) {
            String[] data = row.split(",");
            allUUID.add(data[0]);
            i++;
        }
        csvReader.close();
        return allUUID;
    }

    public  void writeCSV(ArrayList input) throws IOException, URISyntaxException {
        String userKey = "d29cceca579ec417b33bb5902853be92";
        int nbLine = 0;
        int count = 0;
        String content = null;
        for (Object object: input
             ) {

                String uuid = (String) object;
                String cbApiUrl = "https://api.crunchbase.com/v3.1/organizations/"+ uuid + "?user_key="+userKey;
                InputStreamReader is;
                BufferedReader in;
                URL url = new URL(cbApiUrl);
                System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("GET");
                con.setRequestProperty("userId", userKey);
                switch (con.getResponseCode()) {
                    case HttpURLConnection.HTTP_OK:
                        //System.out.println(" **OK**");
                        is = new InputStreamReader (con.getInputStream(), "UTF-8");
                        in = new BufferedReader (is);

                        String line = null;


                        while ((line = in.readLine()) != null) {
                            content = content + line +  "\n";
                            nbLine++;
                            count++;
                            System.out.println("line " + count + " :" + line);
                            ObjectMapper mapper = new ObjectMapper();
                            mapper.configure(
                                DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                            mapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
                            JsonNode jsonNode = mapper.readTree(line);
                            JsonNode dataNode =  jsonNode.get("data");
                            String dataNodeString = dataNode.toString();
                            String propertiesNodeString = dataNode.get("properties").toString();
                            OrganizationEntity organizationEntity = mapper.readValue(propertiesNodeString, OrganizationEntity.class);
                            organizationEntity.setUuid( UUID.fromString(uuid));
                            organizationEntityRepository.save(organizationEntity);
                            System.out.println("object " + organizationEntity );
                        }
                        /*if (nbLine == 10){
                            Long timestamps = new  Date().getTime();
                            FileWriter fw = new FileWriter("C:\\Travail\\vnsdjava\\src\\main\\resources\\csv\\app"+ timestamps.toString()+".txt");
                            BufferedWriter bw = new BufferedWriter(fw);
                            bw.write(content);
                            content = null;
                            nbLine = 0;
                            break;
                        }*/

                        break;
                    case HttpURLConnection.HTTP_GATEWAY_TIMEOUT:
                        System.out.println(" **gateway timeout**");
                        break;
                    case HttpURLConnection.HTTP_UNAVAILABLE:
                        System.out.println("**unavailable**");
                        break;
                    default:
                        System.out.println(" **unknown response code**.");
                        break ; // abort
                }



        }



    }

}
